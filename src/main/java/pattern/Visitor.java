package pattern;

public interface Visitor {
    void visit(Cat cat);
    void visit(Bird bird);
    void visit(Fish fish);
}
