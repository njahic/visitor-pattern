package pattern;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[]args){
        List<VisitableElement> elements = new ArrayList<>();
        elements.add(new Cat());
        elements.add(new Bird());
        elements.add(new Fish());

        for (VisitableElement element: elements){
            element.accept(new FeederVisitor());
            element.accept(new GroomerVisitor());
        }
    }
}
