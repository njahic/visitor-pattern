package pattern;

public class Bird implements VisitableElement{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
