package pattern;

public interface VisitableElement {
    void accept(Visitor visitor);
}
