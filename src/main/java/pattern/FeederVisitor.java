package pattern;

public class FeederVisitor implements Visitor {
    @Override
    public void visit(Cat cat) {
        System.out.println("Feeding milk to cat");
    }

    @Override
    public void visit(Bird bird) {
        System.out.println("Feeding seeds to bird");
    }

    @Override
    public void visit(Fish fish) {
        System.out.println("Feeding algae to fish");
    }
}
