package pattern;

public class Cat implements VisitableElement{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
