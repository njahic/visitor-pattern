package pattern;

public class GroomerVisitor implements Visitor {
    @Override
    public void visit(Cat cat) {
        System.out.println("Brushing cat hair");
    }

    @Override
    public void visit(Bird bird) {
        System.out.println("Clipping bird claws");
    }

    @Override
    public void visit(Fish fish) {
        System.out.println("What the..I'm already clean!");
    }
}
