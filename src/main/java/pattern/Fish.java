package pattern;

public class Fish implements VisitableElement{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
